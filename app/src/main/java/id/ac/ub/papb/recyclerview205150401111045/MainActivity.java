package id.ac.ub.papb.recyclerview205150401111045;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv1;
    public static String TAG = "RV1";
    EditText etNama, etNim;
    Button bt1;

    ArrayList<Mahasiswa> listMahasiswa;
    MahasiswaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        etNama = findViewById(R.id.etNama);
        etNim = findViewById(R.id.etNim);
        bt1 = findViewById(R.id.bt1);
        listMahasiswa = new ArrayList<>();
        adapter = new MahasiswaAdapter(this, listMahasiswa);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        bt1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.nama = etNama.getText().toString();
        mahasiswa.nim = etNim.getText().toString();

        listMahasiswa.add(mahasiswa);
        adapter.notifyDataSetChanged();
    }
//    public ArrayList getData() {
//        ArrayList<Mahasiswa> data = new ArrayList<>();
//        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
//        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
//        for (int i = 0; i < nim.size(); i++) {
//            Mahasiswa mhs = new Mahasiswa();
//            mhs.nim = nim.get(i);
//            mhs.nama = nama.get(i);
//            Log.d(TAG,"getData "+mhs.nim);
//            data.add(mhs);
//        }
//        return data;
//    }
}